package ru.atgroup.tasks.task1;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author entarrion
 *
 */
abstract public class AbstractSrtSetTest {
    private static final String[] DATA_FOR_SET_A = { "0", "1", "2", "3" };
    private static final String[] DATA_FOR_SET_B = { "2", "3", "4", "5", "D" };

    protected static IStrSet setA;
    protected static IStrSet setB;
    protected static IStrSet setC;
    protected static IStrSet setD;

    private static Set<String> arrayToSet(String... array) {
        return new HashSet<String>(Arrays.asList(array));
    }

    @BeforeClass
    public static void beforeClass() {
        Assert.fail("В наследуемых классах необходимо переопределять данный метод, в нем присваивать переменным setA...D их реализации");
    }

    private static Set<String> iStrSetToSet(IStrSet iStrSet) {
        return arrayToSet(iStrSet.getAll());
    }

    /**
     * Инициализация множеств.
     */
    @Before
    public void before() {
        setA.clear();
        setB.clear();
        setC.clear();
        setD.clear();
        for (String item : DATA_FOR_SET_A) {
            setA.add(item);
            setC.add(item);
        }
        for (String item : DATA_FOR_SET_B) {
            setB.add(item);
            setC.add(item);
        }
    }

    @Test
    public void testAdd() {
        String item = "dfsghdgjskasgsdfgsdfghhewrtgfdshdshdfghl";
        Set<String> expected = arrayToSet(DATA_FOR_SET_A);
        Assert.assertEquals(expected, iStrSetToSet(setA));
        setA.add(item);
        Assert.assertNotEquals(expected, iStrSetToSet(setA));
        expected.add(item);
        Assert.assertEquals(expected, iStrSetToSet(setA));
    }

    @Test
    public void testAddAll() {
        Set<String> expected = arrayToSet(DATA_FOR_SET_A);
        setA.addAll(setB);
        Assert.assertNotEquals(expected, iStrSetToSet(setA));
        expected.addAll(arrayToSet(DATA_FOR_SET_B));
        Assert.assertEquals(expected, iStrSetToSet(setA));
    }

    @Test
    public void testDifference() {
        Set<String> expected = arrayToSet(DATA_FOR_SET_A);
        expected.removeAll(arrayToSet(DATA_FOR_SET_B));
        IStrSet difference = setA.difference(setB);
        Assert.assertEquals(expected, iStrSetToSet(difference));
        Assert.assertEquals(arrayToSet(DATA_FOR_SET_A), iStrSetToSet(setA));
        Assert.assertEquals(arrayToSet(DATA_FOR_SET_B), iStrSetToSet(setB));
    }

    @Test
    public void testIntersection() {
        Set<String> expected = arrayToSet(DATA_FOR_SET_A);
        expected.retainAll(arrayToSet(DATA_FOR_SET_B));
        IStrSet intersection = setA.intersection(setB);
        Assert.assertEquals(expected, iStrSetToSet(intersection));
        Assert.assertEquals(arrayToSet(DATA_FOR_SET_A), iStrSetToSet(setA));
        Assert.assertEquals(arrayToSet(DATA_FOR_SET_B), iStrSetToSet(setB));
    }

    @Test
    public void testRemove() {
        String item = DATA_FOR_SET_A[0];
        Set<String> expected = arrayToSet(DATA_FOR_SET_A);
        Assert.assertEquals(expected, iStrSetToSet(setA));
        setA.remove(item);
        Assert.assertNotEquals(expected, iStrSetToSet(setA));
        expected.remove(item);
        Assert.assertEquals(expected, iStrSetToSet(setA));
    }

    @Test
    public void testRemoveAll() {
        Set<String> expected = arrayToSet(DATA_FOR_SET_A);
        Assert.assertEquals(expected, iStrSetToSet(setA));
        setA.removeAll(setB);
        Assert.assertNotEquals(expected, iStrSetToSet(setA));
        expected.removeAll(arrayToSet(DATA_FOR_SET_B));
        Assert.assertEquals(expected, iStrSetToSet(setA));
    }

    @Test
    public void testSubset() {
        Assert.assertTrue(setC.isSubset(setC));
        Assert.assertTrue(setA.isSubset(setC));
        Assert.assertTrue(setB.isSubset(setC));
        Assert.assertTrue(setD.isSubset(setC));
        Assert.assertTrue(setD.isSubset(setA));
        Assert.assertTrue(setD.isSubset(setB));
        Assert.assertFalse(setA.isSubset(setB));
        Assert.assertFalse(setB.isSubset(setA));
        Assert.assertFalse(setC.isSubset(setA));
        Assert.assertFalse(setC.isSubset(setB));
        Assert.assertFalse(setC.isSubset(setD));
        Assert.assertFalse(setA.isSubset(setD));
        Assert.assertFalse(setB.isSubset(setD));
    }

    @Test
    public void testSuperset() {
        Assert.assertTrue(setC.isSuperset(setC));
        Assert.assertTrue(setC.isSuperset(setA));
        Assert.assertTrue(setC.isSuperset(setB));
        Assert.assertTrue(setC.isSuperset(setD));
        Assert.assertTrue(setA.isSuperset(setD));
        Assert.assertTrue(setB.isSuperset(setD));
        Assert.assertTrue(setD.isSuperset(setD));
        Assert.assertFalse(setA.isSuperset(setB));
        Assert.assertFalse(setB.isSuperset(setA));
        Assert.assertFalse(setA.isSuperset(setC));
        Assert.assertFalse(setB.isSuperset(setC));
        Assert.assertFalse(setD.isSuperset(setC));
        Assert.assertFalse(setD.isSuperset(setA));
        Assert.assertFalse(setD.isSuperset(setB));
    }

    @Test
    public void testSymmetricDifference() {
        Set<String> union = arrayToSet(DATA_FOR_SET_A);
        union.addAll(arrayToSet(DATA_FOR_SET_B));
        Set<String> intersection = arrayToSet(DATA_FOR_SET_A);
        intersection.retainAll(arrayToSet(DATA_FOR_SET_B));
        union.removeAll(intersection);
        Set<String> expected = union;
        IStrSet symmetricDifference = setA.symmetricDifference(setB);
        Assert.assertEquals(expected, iStrSetToSet(symmetricDifference));
        Assert.assertEquals(arrayToSet(DATA_FOR_SET_A), iStrSetToSet(setA));
        Assert.assertEquals(arrayToSet(DATA_FOR_SET_B), iStrSetToSet(setB));
    }

    @Test
    public void testUnion() {
        Set<String> expected = arrayToSet(DATA_FOR_SET_A);
        expected.addAll(arrayToSet(DATA_FOR_SET_B));
        IStrSet union = setA.union(setB);
        Assert.assertEquals(expected, iStrSetToSet(union));
        Assert.assertEquals(arrayToSet(DATA_FOR_SET_A), iStrSetToSet(setA));
        Assert.assertEquals(arrayToSet(DATA_FOR_SET_B), iStrSetToSet(setB));
    }
}
