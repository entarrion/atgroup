package ru.atgroup.tasks.task1;

/**
 * Интерфейс для строковых коллекций.
 *
 * @author entarrion
 * @since 22.07.2014
 */
public interface IStrCollection {

    /**
     * Очистить коллекцию.
     */
    void clear();

    /**
     * Получить все элементы коллекции в виде массива.
     *
     * @return все элементы коллекции в виде массива.
     */
    String[] getAll();

    /**
     * Возвращает true, если коллекция не содержит элементов, иначе false.
     *
     * @return true, если коллекция не содержит элементов, иначе false
     */
    boolean isEmpty();
}
