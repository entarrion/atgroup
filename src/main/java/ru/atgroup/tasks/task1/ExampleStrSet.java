package ru.atgroup.tasks.task1;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 * Класс реализующий интерфейс IStrSet. Данный класс основа на HashSet и создан
 * для проверки результатов реализации множества на списках/массивах.
 *
 * @author entarrion
 *
 */
public class ExampleStrSet implements IStrSet {

    private Set<String> set;

    public ExampleStrSet() {
        setSet(new HashSet<String>());
    }

    public ExampleStrSet(IStrCollection items) {
        this(items.getAll());
    }

    public ExampleStrSet(String... items) {
        this();
        getSet().addAll(Arrays.asList(items));
    }

    @Override
    public void add(String item) {
        getSet().add(item);
    }

    @Override
    public void addAll(IStrCollection items) {
        getSet().addAll(Arrays.asList(items.getAll()));
    }

    @Override
    public void clear() {
        getSet().clear();
    }

    @Override
    public IStrSet difference(IStrSet other) {
        Set<String> tmp = new HashSet<String>(this.getSet());
        tmp.removeAll(Arrays.asList(other.getAll()));
        return new ExampleStrSet(tmp.toArray(new String[tmp.size()]));
    }

    @Override
    public String[] getAll() {
        return getSet().toArray(new String[getSet().size()]);
    }

    public Set<String> getSet() {
        return set;
    }

    @Override
    public IStrSet intersection(IStrSet other) {
        Set<String> tmp = new HashSet<String>(this.getSet());
        tmp.retainAll(Arrays.asList(other.getAll()));
        return new ExampleStrSet(tmp.toArray(new String[tmp.size()]));
    }

    @Override
    public boolean isEmpty() {
        return getSet().isEmpty();
    }

    @Override
    public boolean isSubset(IStrSet other) {
        Set<String> tmp = new HashSet<String>(Arrays.asList(other.getAll()));
        return tmp.containsAll(getSet());
    }

    @Override
    public boolean isSuperset(IStrSet other) {
        return getSet().containsAll(Arrays.asList(other.getAll()));
    }

    @Override
    public void remove(String item) {
        getSet().remove(item);
    }

    @Override
    public void removeAll(IStrCollection items) {
        getSet().removeAll(Arrays.asList(items.getAll()));
    }

    public void setSet(Set<String> set) {
        this.set = set;
    }

    @Override
    public IStrSet symmetricDifference(IStrSet other) {
        IStrSet intersection = this.intersection(other);
        IStrSet union = this.union(other);
        return union.difference(intersection);
    }

    @Override
    public IStrSet union(IStrSet other) {
        Set<String> tmp = new HashSet<String>(this.getSet());
        tmp.addAll(Arrays.asList(other.getAll()));
        return new ExampleStrSet(tmp.toArray(new String[tmp.size()]));
    }

}
